var gulp        = require('gulp'), // Подключаем Gulp
    cache       = require('gulp-cache'), // Подключаем библиотеку кеширования
    tiny 		= require('gulp-tinypng-nokey');

gulp.task('img', function() {
    return gulp.src('source/**/*.+(jpg|jpeg|png)') // Берем все изображения из папки
        .pipe(tiny())
        .pipe(gulp.dest('compressed')); // Выгружаем на продакшен
});

gulp.task('default', ['img']);